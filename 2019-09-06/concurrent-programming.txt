Goals of concurrency
- Get more done in less time
- Multiple independent tasks
- Repeat same task
- Solution Parallelism


Problems
- Starting/maintaining processes
- Communication between processes 
  - remote procedure call 
  - message passing
- Shared Resources
  - Race Conditions
  - Starvation 
  - Livelock
  - Deadlock


Approaches
- OOP
  - Failure Prevention
  - Monitor
  - Mutual Exclusion
  - Prioritize Algorithms
- Functional
  - Support Process Death


Features/Drawbacks
- OOP
  - Bring data and behavior together
  - simulation of behavior
  - overhead: Java allocates 512kb/thread
  - Requires thread pools
  - Shared heap
  - Hard memory limits
- Functional
  - Separate data from behaviors
  - Data manipulation
  - Overhead: Elixer 512b/thread
  - Processes easily accessible
  - Individual Private Heap
  - Unbound memory


Use 2 processes if:
- Don't want failure of func A to affect outcome of B
- Don't want latency of func A to affect latency of B


Use multiple processes if
- Manage message queue; spin up a process for each individual message 


Use single process if:
- Desire "atomicity"


BEAM uses OS/natural garbage collection
